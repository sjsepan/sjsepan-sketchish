# Sketchish Theme

Sketchish color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-sketchish_code.png](./images/sjsepan-sketchish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-sketchish_codium.png](./images/sjsepan-sketchish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-sketchish_codedev.png](./images/sjsepan-sketchish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-sketchish_ads.png](./images/sjsepan-sketchish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-sketchish_theia.png](./images/sjsepan-sketchish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-sketchish_positron.png](./images/sjsepan-sketchish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/13/2025
