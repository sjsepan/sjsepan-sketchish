# Sketchish Color Theme - Change Log

## [0.1.4]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.1.3]

- update readme and screenshot

## [0.1.2]

- fix source control graph badge colors: dim scmGraph.historyItemRefColor
- fix titlebar border, FG/BG in custom mode
- focusborder to match theme
- define window border in custom mode
- fix menu FG/BG

## [0.1.1]

- fix manifest and pub WF

## [0.1.0]

- button BG color
- fix manifest repo links
- retain v0.0.2 for those who prefer earlier style

## [0.0.2]

- make lst act sel BG transparent

## [0.0.1]

- Initial release
